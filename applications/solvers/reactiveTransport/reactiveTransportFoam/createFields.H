    //alpha1=1.0 is set for the reaction module
    volScalarField alpha1
    (
        IOobject
        (
            "alpha1",
            runTime.timeName(),
            mesh
        ),
		mesh,
        scalar(1.0)
    );

    Info<< "Reading field U\n" << endl;

    volVectorField U
    (
        IOobject
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    Info<< "Reading transportProperties\n" << endl;

    IOdictionary transportProperties
    (
        IOobject
        (
            "transportProperties",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

#   include "createPhi.H"

#   include "createY.H"

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "Reading field Surf\n" << endl;
    volScalarField Surf
    (
        IOobject
        (
            "Surf",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

	Info << "Reading kinetic phases dictionary" << endl;
	wordList kineticPhasesNames;
	if (transportProperties.found("kineticPhases"))
	{
		Info << "Reading kinetic phases dictionary" << endl;
		const dictionary& kineticPhasesDict = transportProperties.subDict("kineticPhases");
		Info << "Reading kinetic phases names\n" << endl;
		kineticPhasesNames = kineticPhasesDict.toc();
	}

    speciesTable kineticPhases
    (
       kineticPhasesNames
    );

	PtrList<volScalarField> R(kineticPhases.size());
	forAll(kineticPhases, i)
	{
		R.set
		(
			i,
			new volScalarField
			(
				IOobject
				(
					"R_" + kineticPhases[i],
					mesh.time().timeName(),
					mesh,
					IOobject::NO_READ,
					IOobject::NO_WRITE
				),
				mesh,
				dimensionedScalar("R_"+kineticPhases[i], dimensionSet(0,-2,-1,0,1,0,0), 0.0)
			)
		);
	}

	wordList selectedOutputNames;
	if (transportProperties.found("selectedOutput"))
	{
		Info << "Reading selected output" << endl;
		const dictionary& selectedOutputDict = transportProperties.subDict("selectedOutput");
		Info << "Reading solution species name\n" << endl;
	    selectedOutputNames = selectedOutputDict.toc();
	}


	Info << "Create selected output vector\n" << endl;
	PtrList<volScalarField> sOut(selectedOutputNames.size());

	forAll(selectedOutputNames, i)
	{
		sOut.set
		(
			i,
			new volScalarField
			(
				IOobject
				(
					selectedOutputNames[i],
					mesh.time().timeName(),
					mesh,
					IOobject::MUST_READ,
					IOobject::AUTO_WRITE
				),
				mesh
			)
		);
	}

	//surface potential
    volScalarField psi
    (
        IOobject
        (
            "psi",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
		dimensionedScalar("psi", dimensionSet(1,2,-3,0,0,-1,0), 0.0)
    );

	//ionic strength
    volScalarField I
    (
        IOobject
        (
            "I",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
		dimensionedScalar("I", dimMoles/dimVolume, 0.0)
    );
