#!/bin/bash

set -e

rm -rf 0.* *e-* 1* 2* 3* 4* 5* 6* 7* 8* 9*


rm -rf constant/cell* constant/point*
rm -f constant/polyMesh/boundary
rm -rf constant/polyMesh/cell* constant/polyMesh/point* constant/polyMesh/face* 
rm -rf constant/polyMesh/neighbour*  constant/polyMesh/owner*
rm -rf processor*
rm -f constant/polyMesh/*.gz
rm -f 0/cellToRegion
rm -rf 0/polyMesh/

