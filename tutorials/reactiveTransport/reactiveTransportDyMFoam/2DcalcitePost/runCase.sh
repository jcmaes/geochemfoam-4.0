#!/bin/bash

set -e

blockMesh
cp system/snappyHexMeshDict1 system/snappyHexMeshDict
snappyHexMesh -overwrite
cp system/snappyHexMeshDict2 system/snappyHexMeshDict
snappyHexMesh -overwrite
cp system/snappyHexMeshDict3 system/snappyHexMeshDict
snappyHexMesh -overwrite
createPatch -overwrite
transformPoints -scale '(1e-6 1e-6 1e-6)'
decomposePar
mpiexec -np 6 reactiveTransportDyMFoam -parallel
reconstructPar
rm -rf processor*
